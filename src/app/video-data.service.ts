import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

interface View {
  age: number;
  region: string;
  date: string;
};

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: View[];
}

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideo(id: string): Observable<Video> {
    return this.http.get<Video>('http://localhost:8085/videos/' + id);
  }

  loadVideos(): Observable<Video[]> {
    return this.http
      .get<Video[]>('http://localhost:8085/videos')
      .pipe(
//        tap((videos: Video[]) => console.log('Raw video list from server:', videos)),
        map((videos: Video[]) => videos.slice(0, 3)),
//        tap((videos: Video[]) => console.log('Sliced video list:', videos)),
//        map((videos: Video[]) => videos.map( video => ({ ...video, title: video.title.toUpperCase() }) ) ),
//        tap((videos: Video[]) => console.log('After mangling titles:', videos)),
      );
  }
}
