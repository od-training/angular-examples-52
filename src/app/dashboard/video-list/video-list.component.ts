import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Video } from '../../video-data.service';

@Component({
  selector: 'video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];
  @Input() currentVideoId: string;

  constructor() { }

  ngOnInit() {
  }
}
