import { Component, OnChanges, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnChanges {

  @Input() videoId: string;

  video$: Observable<Video>;

  constructor(private svc: VideoDataService) { }

  ngOnChanges() {
    if (this.videoId) {
      this.video$ = this.svc.loadVideo(this.videoId);
    }
  }
}
