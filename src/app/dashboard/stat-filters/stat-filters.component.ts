import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit {

  fg: FormGroup;

  constructor(fb: FormBuilder) {
    this.fg = fb.group({
      partialVideoTitle: ['', Validators.minLength(3)]
    });
  }

  ngOnInit() {
  }

}
