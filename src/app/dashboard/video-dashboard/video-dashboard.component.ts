import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Video, VideoDataService } from '../../video-data.service';

@Component({
  selector: 'video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {

  videoData$: Observable<Video[]>;
  videoId$: Observable<string>;

  constructor(
    private svc: VideoDataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.videoData$ = this.svc.loadVideos();

    this.videoId$ = this.route.queryParams.pipe(
      tap(qparams => console.log('query params:', qparams)),
      map(qparams => qparams['id'])
    );
  }
}
